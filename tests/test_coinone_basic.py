import base64
import hashlib
import hmac
import json
import time
import httplib2


ACCESS_TOKEN = 'c13e466f-172c-40bb-9ea1-5afe2ce58d26'
SECRET_KEY = bytes('aad90499-acb8-4bb9-8ba1-6483ede842b4', 'utf-8')


def get_encoded_payload(payload):
    print(payload)
    payload['nonce'] = int(time.time() * 1000)
    print(payload)
    dumped_json = json.dumps(payload)
    print(dumped_json)
    encoded_json = base64.b64encode(bytes(dumped_json, 'utf-8'))
    print(encoded_json)
    return encoded_json


def get_signature(encoded_payload):
    signature = hmac.new(SECRET_KEY, encoded_payload, hashlib.sha512)
    return signature.hexdigest()


def get_response(action, payload):
    url = '{}{}'.format('https://api.coinone.co.kr/', action)

    encoded_payload = get_encoded_payload(payload)

    headers = {
        'Content-type': 'application/json',
        'X-COINONE-PAYLOAD': encoded_payload,
        'X-COINONE-SIGNATURE': get_signature(encoded_payload),
    }

    http = httplib2.Http()
    response, content = http.request(url, 'POST', body=encoded_payload, headers=headers)

    return content


print(get_response(action='v2/order/limit_buy', payload={
    'access_token': ACCESS_TOKEN,
    'price': '338.0',
    'qty': '10',
    'currency': 'xrp',
}))