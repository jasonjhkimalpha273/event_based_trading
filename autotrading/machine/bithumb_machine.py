import requests
import time
import math
from autotrading.machine.base_machine import Machine
import configparser
import json
import base64
import hashlib
import hmac
import urllib


class BithumbMachine(Machine):
    """
    Class to trade in the Bithumb Exchange
    BASE_API_URL: Basic REST API Request URL
    TRADE_CURRENCY_TYPE: Tradable Currency in Bithumb
    """
    BASE_API_URL = "https://api.bithumb.com"
    TRADE_CURRENCY_TYPE = ["BTC", "ETH", "DASH", "LTC", "ETC", "XRP", "BCH", "XMR", "ZEC", "QTUM", "BTG", "EOS",
                           "ICX", "TRX", "ELF", "MITH", "MCO", "OMG", "KNC", "GNT", "ZIL", "ETHOS", "PAY", "WAX",
                           "POWR", "LRC", "GTO", "STEEM", "STRAT", "AE", "ZRX", "REP", "XEM", "SNT", "ADA", "PPT",
                           "CTXC", "BAT", "WTC", "CMT", "THETA", "POLY", "LOOM", "WAVES", "ITC", "TRUE", "ABT",
                           "LINK", "SALT", "PST", "RNT", "ENJ", "PLY", "VET", "MTL", "RDN", "INS", "IOST", "OCN",
                           "TMTG", "QKC", "BZNT", "ARN", "HDAC", "NPXS", "LBA", "WET", "AMO", "BSV", "ROM", "APIS",
                           "DAC", "DACC", "AUTO", "ORBS", "VALOR", "CON", "ANKR", "MIX", "LAMB", "CRO", "FX", "CHR",
                           "MXC", "FAB", "OGO", "DVP", "FNB", "FZZ", "TRV", "DAD", "HC", "BCD", "XVG", "XLM", "PIVX",
                           "ETZ", "GXC", "BHP", "BTT", "HYC"]

    def __init__(self):
        """
        Initiative Method to call the BithumbMachine Class
        Retrieve data from conf/config.ini
        """
        configFileName = '/Users/jaehyunkim/Documents/Workspace/alphanonce_eventdriven_autotrading/conf/config.ini'
        config = configparser.ConfigParser()
        config.read(configFileName)
        self.CLIENT_ID = config['BITHUMB']['connect_key']
        self.CLIENT_SECRET = config['BITHUMB']['secret_key']
        self.USER_NAME = config['BITHUMB']['username']
        # self.access_token = None
        # self.refresh_token = None
        # self.token_type = None

    def get_username(self):
        return self.USER_NAME

    def get_token(self):
        pass

    def set_token(self):
        pass

    def get_nonce(self):
        return self.usecTime()  # str(int(time.time()))

    def get_ticker(self, currency_type=None):
        """
        Retrieve last executed tick data from orderbook of token
        :param currency_type:  Retrieve type of currency as param TRADE_CURRENCY_TYPE
        :return: Results returned as Dictionary, fields : timestamp, last, bid, ask, high, low,
                 volume, raised exception if nothing is to be returned
        """
        if currency_type is None:
            raise Exception('Need to currency type')
        if currency_type not in self.TRADE_CURRENCY_TYPE:
            raise Exception('Not support currency type')
        time.sleep(1)
        ticker_api_path = "/public/ticker/{currency}".format(currency=currency_type)
        url_path = self.BASE_API_URL + ticker_api_path
        res = requests.get(url_path)
        response_json = res.json()
        result = {}
        result["timestamp"] = str(response_json['data']["date"])
        result["last"] = response_json['data']["closing_price"]
        result["bid"] = response_json['data']["buy_price"]
        result["ask"] = response_json['data']["sell_price"]
        result["high"] = response_json['data']["max_price"]
        result["low"] = response_json['data']["min_price"]
        result["volume"] = response_json['data']["volume_1day"]
        return result

    def get_filled_orders(self, currency_type=None):
        """
        :param currency_type: String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :return: All executed information returned in dictionary list type
        """
        if currency_type is None:
            raise Exception("Need to currency_type")
        if currency_type not in self.TRADE_CURRENCY_TYPE:
            raise Exception('Not support currency type')
        time.sleep(1)
        params = {'offset': 0, 'count': 100}
        orders_api_path = "/public/recent_transactions/{currency}".format(currency=currency_type)
        url_path = self.BASE_API_URL + orders_api_path
        res = requests.get(url_path, params=params)
        result = res.json()
        return result

    def microtime(self, get_as_float=False):
        """
        Method that retrieves timestamp which is used to create the nonce value
        :param get_as_float(boolean): Input is given either to choose return type as float or int
        :return: type int or float returned
        """
        if get_as_float:
            return time.time()
        else:
            return '%f %d' % math.modf(time.time())

    def usecTime(self):
        """
        Method to manifacture data acquired after calling method microtime
        :return : Returns Type Timestamp
        """
        mt = self.microtime(False)
        mt_array = mt.split(" ")[:2]
        return mt_array[1] + mt_array[0][2:5]

    def get_signature(self, encoded_payload, secret_key):
        """
        :param encoded_payload: String Type, Payload Value that is incoded
        :param secret_key: String Type, Secret_key used to write signature to documents
        :return: Return data after secret_key is written
        """
        signature = hmac.new(secret_key, encoded_payload, hashlib.sha512)
        api_sign = base64.b64encode(signature.hexdigest().encode('utf-8'))
        return api_sign

    def get_wallet_status(self, currency_type=None):
        """
        Method to retrieve wallet information
        :param: Type of Currency
        :return: Returns Wallet Info via dictionary type
        """
        if currency_type is None:
            raise Exception("Need to Check currency_type")
        if currency_type not in self.TRADE_CURRENCY_TYPE:
            raise Exception('No support for this currency type')
        time.sleep(1)
        endpoint = "/info/balance"
        url_path = self.BASE_API_URL + endpoint

        endpoint_item_array = {
            "endpoint": endpoint,
            "currency": currency_type
        }
        uri_array = dict(endpoint_item_array)  # Concatenate the two arrays.
        str_data = urllib.parse.urlencode(uri_array)
        nonce = self.get_nonce()
        data = endpoint + chr(0) + str_data + chr(0) + nonce
        utf8_data = data.encode('utf-8')

        key = self.CLIENT_SECRET
        utf8_key = key.encode('utf-8')

        headers = {'Content-Type': 'application/x-www-form-urlencoded',
                   'Api-Key': self.CLIENT_ID,
                   'Api-Sign': self.get_signature(utf8_data, bytes(utf8_key)),
                   'Api-Nonce': nonce
                   }

        res = requests.post(url_path, headers=headers, data=str_data)
        result = res.json()
        return result["data"]

    def get_list_my_orders(self, currency_type=None):
        """
        Method to retrieve Orders that the user sent
        :param currency_type: String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :return: Returns the user's balance by Currency Type
        """
        if currency_type is None:
            raise Exception("Need to currency_type")
        if currency_type not in self.TRADE_CURRENCY_TYPE:
            raise Exception('Not support currency type')
        time.sleep(1)
        endpoint = "/info/orders"
        url_path = self.BASE_API_URL + endpoint

        endpoint_item_array = {
            "endpoint": endpoint,
            "currency": currency_type
        }

        uri_array = dict(endpoint_item_array)  # Concatenate the two arrays.
        str_data = urllib.parse.urlencode(uri_array)
        nonce = self.get_nonce()
        data = endpoint + chr(0) + str_data + chr(0) + nonce
        utf8_data = data.encode('utf-8')

        key = self.CLIENT_SECRET
        utf8_key = key.encode('utf-8')

        headers = {'Content-Type': 'application/x-www-form-urlencoded',
                   'Api-Key': self.CLIENT_ID,
                   'Api-Sign': self.get_signature(utf8_data, bytes(utf8_key)),
                   'Api-Nonce': nonce}

        res = requests.post(url_path, headers=headers, data=str_data)
        result = res.json()
        return result["data"]

    def get_my_order_status(self, currency_type=None, order_id=None):
        """
        Method to retrieve all order info
        :param currency_type:  String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :param order_id: Transaction_ID
        :return: Precise info returned via order_id accordingly
        """
        if currency_type is None:
            raise Exception("Need to currency_type")
        if currency_type not in self.TRADE_CURRENCY_TYPE:
            raise Exception('Not support currency type')
        time.sleep(1)
        endpoint = "/info/order_detail"
        url_path = self.BASE_API_URL + endpoint

        endpoint_item_array = {
            "endpoint": endpoint,
            "currency": currency_type
        }

        uri_array = dict(endpoint_item_array)  # Concatenate the two arrays.
        str_data = urllib.parse.urlencode(uri_array)
        nonce = self.get_nonce()
        data = endpoint + chr(0) + str_data + chr(0) + nonce
        utf8_data = data.encode('utf-8')

        key = self.CLIENT_SECRET
        utf8_key = key.encode('utf-8')

        headers = {'Content-Type': 'application/x-www-form-urlencoded',
                   'Api-Key': self.CLIENT_ID,
                   'Api-Sign': self.get_signature(utf8_data, bytes(utf8_key)),
                   'Api-Nonce': nonce}

        res = requests.post(url_path, headers=headers, data=str_data)
        result = res.json()
        return result["data"]

    def buy_order(self, currency_type=None, price=None, qty=None, order_type="limit"):
        """
        Method to Execute Limit Buy Order. Please note that minimum order amount differs by currency type
        :param currency_type: String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :param price: String Type, 1 Order Amount Equivalent KRW Value
        :param qty: Order Amount
        :param order_type: All order types are limit orders. No Taker Orders
        :return: Returns the status of Orders
        """
        if currency_type is None:
            raise Exception("Need to check currency_type")
        if currency_type not in self.TRADE_CURRENCY_TYPE:
            raise Exception('No support for this currency type')
        time.sleep(1)
        endpoint = "/trade/place"
        url_path = self.BASE_API_URL + endpoint

        endpoint_item_array = {
            "endpoint": endpoint,
            "order_currency": currency_type,
            "payment_currency": "KRW",
            "units": qty,
            "price": price,
            "type": "bid"
        }

        uri_array = dict(endpoint_item_array)  # Concatenate the two arrays.
        str_data = urllib.parse.urlencode(uri_array)
        nonce = self.get_nonce()
        data = endpoint + chr(0) + str_data + chr(0) + nonce
        utf8_data = data.encode('utf-8')

        key = self.CLIENT_SECRET
        utf8_key = key.encode('utf-8')

        headers = {'Content-Type': 'application/x-www-form-urlencoded',
                   'Api-Key': self.CLIENT_ID,
                   'Api-Sign': self.get_signature(utf8_data, bytes(utf8_key)),
                   'Api-Nonce': nonce}

        res = requests.post(url_path, headers=headers, data=str_data)
        result = res.json()
        return result

    def sell_order(self, currency_type=None, price=None, qty=None, order_type="limit"):
        """
        Method to Execute Limit Sell Order. Please note that minimum order amount differs by currency type
        :param currency_type:  String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :param price: String Type, 1 Order Amount Equivalent KRW Value
        :param qty: Order Amount
        :param order_type: All order types are limit orders. No Taker Orders
        :return: Returns the status of Orders
        """
        if currency_type is None:
            raise Exception("Need to currency_type")
        if currency_type not in self.TRADE_CURRENCY_TYPE:
            raise Exception('Not support currency type')
        time.sleep(1)
        endpoint = "/trade/place"
        url_path = self.BASE_API_URL + endpoint

        endpoint_item_array = {
            "endpoint": endpoint,
            "order_currency": currency_type,
            "payment_currenct": "KRW",
            "units": qty,
            "price": price,
            "type": "ask"
        }

        uri_array = dict(endpoint_item_array)  # Concatenate the two arrays.
        str_data = urllib.parse.urlencode(uri_array)
        nonce = self.get_nonce()
        data = endpoint + chr(0) + str_data + chr(0) + nonce
        utf8_data = data.encode('utf-8')

        key = self.CLIENT_SECRET
        utf8_key = key.encode('utf-8')

        headers = {'Content-Type': 'application/x-www-form-urlencoded',
                   'Api-Key': self.CLIENT_ID,
                   'Api-Sign': self.get_signature(utf8_data, bytes(utf8_key)),
                   'Api-Nonce': nonce}

        res = requests.post(url_path, headers=headers, data=str_data)
        result = res.json()
        return result

    def cancel_order(self, currency_type=None, order_type=None, order_id=None):
        """
        Method to Execute Cancel Orders
        :param currency_type: String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :param order_id: String Type, Order Info that we are trying to cancel
        :return: Status of Order
        """
        if currency_type is None:
            raise Exception("Need to currency_type")
        if currency_type not in self.TRADE_CURRENCY_TYPE:
            raise Exception('Not support currency type')
        time.sleep(1)
        endpoint = "/trade/cancel"
        url_path = self.BASE_API_URL + endpoint

        endpoint_item_array = {
            "endpoint": endpoint,
            "currency": currency_type,
            "type": order_type,
            "order_id": order_id
        }

        uri_array = dict(endpoint_item_array)  # Concatenate the two arrays.
        str_data = urllib.parse.urlencode(uri_array)
        nonce = self.get_nonce()
        data = endpoint + chr(0) + str_data + chr(0) + nonce
        utf8_data = data.encode('utf-8')

        key = self.CLIENT_SECRET
        utf8_key = key.encode('utf-8')

        headers = {'Content-Type': 'application/x-www-form-urlencoded',
                   'Api-Key': self.CLIENT_ID,
                   'Api-Sign': self.get_signature(utf8_data, bytes(utf8_key)),
                   'Api-Nonce': nonce}

        res = requests.post(url_path, headers=headers, data=str_data)
        result = res.json()
        return result

    def __repr__(self):
        return "(Bithumb %s)" % self.USER_NAME

    def __str__(self):
        return str("Bithumb")
