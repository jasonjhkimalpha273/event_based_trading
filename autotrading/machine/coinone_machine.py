import requests
import time
from autotrading.machine.base_machine import Machine
import configparser
import base64
import json
import hashlib
import hmac

class CoinOneMachine(Machine):
    """
    Class to trade in the Coinone Exchange
    BASE_API_URL: Basic REST API Request URL
    TRADE_CURRENCY_TYPE: Tradable Currency in Coinone
    """
    BASE_API_URL = "https://api.coinone.co.kr"
    TRADE_CURRENCY_TYPE = ["btc", "eth", "bch", "bch", "xrp", "eos", "bsv", "atom", "pxl", "trx", "qtum",
                           "vnt", "luna", "ltc", "amo", "xlm", "iota", "zil", "etc", "lamb", "egg", "temco", "dappt",
                           "neo", "spin", "hum", "pib", "prom", "xtz", "hint", "btt", "dad", "abl", "cosm", "omg",
                           "matic", "cpt", "enj", "ksc", "ont", "knc", "btg", "ankr", "data", "orbs", "ong", "zrx",
                           "tfuel", "gas", "rep", "celr", "bat", "theta"]


    def __init__(self):
        """
        Initiative Method to call the CoinOneMachine Class
        Retrieves data fron conf/config.ini : access_token and secret_key
        """
        self.configFileName = '/Users/jaehyunkim/Documents/Workspace/alphanonce_eventdriven_autotrading/conf/config.ini'
        self.config = configparser.ConfigParser()
        self.config.read(self.configFileName)
        self.access_token = self.config['COINONE']['access_token']
        self.secret_key = self.config['COINONE']['secret_key']
        self.username = None
        self.expire = None
        self.refresh_token = None

    def get_username(self):
        if self.username is None:
            return None
        else:
            return self.username

    def get_nonce(self):
        return int(time.time()*1000)

    def get_token(self):
        """
        Method to retrieve Access Token Info

        :return: Returns Access Token if available , raises Exception
        """

        if self.access_token is not None:
            return self.access_token

        else:
            raise Exception("Please designate your Set_Token")


    def set_token(self, grant_type = "refresh_token"):
        """
        Method to Create Access Token
        :param grant_type: Either refresh_token, access_token, expire
        :return: expire, access_token, refresh_token, Raises: If grant_type != refresh_token or password raise exception
        """
        token_api_path = "/oauth/refresh_token"
        url_path = self.BASE_API_URL + token_api_path
        self.expire = 3600

        if grant_type == "refresh_token":
            headers = {"content-type":"application/x-www-form-urlencoded"}
            data = {"access_token":self.access_token}
            res = requests.post(url_path, headers = headers, data = data)
            result = res.json()
            if result["result"] == "success":
                config = configparser.ConfigParser()
                config.read(self.configFileName)
                self.access_token = result["accessToken"]
                config["COINONE"]["access_token"] = self.access_token
                with open(self.configFileName, 'w') as configfile:
                    config.write(configfile)
            else:
                raise Exception("Failed launching set_token")

        else:
            self.access_token = self.config['COINONE']['access_token']
        return self.expire, self.access_token, self.refresh_token

    def get_ticker(self, currency_type = None):
        """
        Method to retrieve the last tick information
        :param currency_type:
        :return: All Results are returned in Dictionary Type. Fields: Timestamp, last, bid, ask high, low, volume. If No currency_type exception Raised
        """
        ticker_api_path = '/ticker/'
        url_path = self.BASE_API_URL + ticker_api_path
        params = {"currency": currency_type}
        res = requests.get(url_path, params=params)
        response_json = res.json()
        result = {}
        result["timestamp"] = str(response_json["timestamp"])
        result["last"] = response_json["last"]
        result["high"] = response_json["high"]
        result["low"] = response_json["low"]
        result["volume"] = response_json["volume"]
        return result


    def get_filled_orders(self, currency_type = None, per = "Minute"):
        pass

    def get_signature(self, encoded_payload, secret_key):
        """
        :param encoded_payload: String Type, Payload Value that is incoded
        :param secret_key: String Type, Secret_key used to write signature to documents
        :return: Return data after secret_key is written
        """
        signature = hmac.new(secret_key, encoded_payload, hashlib.sha512)
        return signature.hexdigest()


    def get_encoded_payload(self, payload):
        """
        Method to incode payloads
        :param payload: String Type, Payloads to be incoded
        :return: Returns Incoded Payloads
        """

        dumped_json = json.dumps(payload)
        encoded_json = base64.b64encode(bytes(dumped_json, 'utf-8'))

        return encoded_json


    def get_wallet_status(self):
        """
        Method to retrieve wallet information
        :return: Returns Wallet Info via dictionary type
        """
        time.sleep(1)
        wallet_status_api_path = "/v2/account/balance"
        url_path = self.BASE_API_URL + wallet_status_api_path
        payload = {
            "access_token": self.access_token,
            "nonce" : self.get_nonce()

        }
        encoded_payload = self.get_encoded_payload(payload)
        signature = self.get_signature(encoded_payload, bytes(self.secret_key,'utf-8'))

        headers = {'Content-type' : 'application/json',
                   'X-COINONE-PAYLOAD' : encoded_payload,
                   'X-COINONE-SIGNATURE' : signature
        }

        res = requests.post(url_path, headers = headers, data = payload)
        result = res.json()
        return result

    def get_list_my_orders(self, currency_type=None):
        """
        Method to retrieve Orders that the user sent
        :param currency_type: String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :return: Returns the user's balance by Currency Type
        """
        time.sleep(1)
        list_api_path = "/v2/order/limit_orders/"
        url_path = self.BASE_API_URL + list_api_path

        payload = {
            "access_token": self.access_token,
            "currency": currency_type,
            'nonce': self.get_nonce()
        }

        encoded_payload = self.get_encoded_payload(payload)
        signature = self.get_signature(encoded_payload, self.secret_key.encode('utf-8'))

        headers = {'Content-type': 'application/json',
                   'X-COINONE-PAYLOAD': encoded_payload,
                   'X-COINONE-SIGNATURE': signature}

        res = requests.post(url_path, headers=headers, data=payload)
        result = res.json()
        return result



    def get_my_order_status(self, currency_type = None, order_id = None):
        """
        Method to retrieve all order info
        :param currency_type:  String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :param order_id: Transaction_ID
        :return: Precise info returned via order_id accordingly
        """
        list_api_path = "/v2/order/order/info/"
        url_path = self.BASE_API_URL + list_api_path
        payload = {
            "access_token": self.access_token,
            "currency": currency_type,
            "order_id": order_id,
            'nonce': self.get_nonce()
        }

        encoded_payload = self.get_encoded_payload(payload)
        signature = self.get_signature(encoded_payload, self.secret_key.encode('utf-8'))

        headers = {'Content-type': 'application/json',
                   'X-COINONE-PAYLOAD': encoded_payload,
                   'X-COINONE-SIGNATURE': signature}

        res = requests.post(url_path, headers=headers, data=payload)
        result = res.json()
        return result

    def buy_order(self, currency_type=None, price=None, qty=None, order_type = "limit"):
        """
        Method to Execute Limit Buy Order. Please note that minimum order amount differs by currency type
        :param currency_type: String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :param price: String Type, 1 Order Amount Equivalent KRW Value
        :param qty: Order Amount
        :param order_type: All order types are limit orders. No Taker Orders
        :return: Returns the status of Orders
        """
        if order_type != "limit":
            raise Exception("Please note that Coinone only supports limit order type.")
        time.sleep(1)
        buy_limit_api_path = "/v2/order/limit_buy/"
        url_path = self.BASE_API_URL + buy_limit_api_path

        payload = {
            "access_token": self.access_token,
            "price": int(price),
            "qty": float(qty),
            "currency": currency_type,
            'nonce': self.get_nonce()
        }

        encoded_payload = self.get_encoded_payload(payload)
        signature = self.get_signature(encoded_payload, bytes(self.secret_key, 'utf-8'))
        headers = {'Content-type': 'application/json',
                   'X-COINONE-PAYLOAD': encoded_payload,
                   'X-COINONE-SIGNATURE': signature}
        """
        http = httplib2.Http()
        response, content = http.request(url_path, 'POST', body = encoded_payload, headers = headers)
        return content
        """

        res = requests.post(url_path, headers=headers, data=payload)
        result = res.json()
        return result

    def sell_order(self, currency_type=None, price=None, qty=None, order_type="limit"):
        """
        Method to Execute Limit Sell Order. Please note that minimum order amount differs by currency type
        :param currency_type:  String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :param price: String Type, 1 Order Amount Equivalent KRW Value
        :param qty: Order Amount
        :param order_type: All order types are limit orders. No Taker Orders
        :return: Returns the status of Orders
        """
        if order_type != "limit":
            raise Exception("Please note that Coinone only supports limit order type.")
        time.sleep(1)
        sell_limit_api_path = "/v2/order/limit_sell/"
        url_path = self.BASE_API_URL + sell_limit_api_path

        payload = {
            "access_token": self.access_token,
            "price": int(price),
            "qty": float(qty),
            "currency": currency_type,
            'nonce': self.get_nonce()
        }

        encoded_payload = self.get_encoded_payload(payload)
        signature = self.get_signature(encoded_payload, bytes(self.secret_key, 'utf-8'))

        headers = {'Content-type': 'application/json',
                   'X-COINONE-PAYLOAD': encoded_payload,
                   'X-COINONE-SIGNATURE': signature}

        res = requests.post(url_path, headers=headers, data=payload)
        result = res.json()
        return result

    def cancel_order(self, currency_type=None, price = None, qty = None, order_type=None, order_id=None):
        """
        Method to Execute Cancel Orders
        :param currency_type: String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :param order_id: String Type, Order Info that we are trying to cancel
        :return: Status of Order
        """
        if currency_type is None or order_type is None or order_id is None:
            raise Exception("Please Check your Params")
        time.sleep(1)
        cancel_api_path = "/v2/order/cancel/"
        url_path = self.BASE_API_URL + cancel_api_path

        payload = {
            "access_token": self.access_token,
            "order_id": order_id,
            "price" : int(price),
            "qty": float(qty),
            "is_ask": 1 if order_type is "sell" else 0,
            "currency": currency_type,
            'nonce': self.get_nonce()
        }

        encoded_payload = self.get_encoded_payload(payload)
        signature = self.get_signature(encoded_payload, bytes(self.secret_key, 'utf-8'))

        headers = {'Content-type': 'application/json',
                   'X-COINONE-PAYLOAD': encoded_payload,
                   'X-COINONE-SIGNATURE': signature}

        res = requests.post(url_path, headers=headers, data=payload)
        result = res.json()
        return result

    def __repr__(self):
        return "(CoinOne %s)" % self.username

    def __str__(self):
        return str("CoinOne")













