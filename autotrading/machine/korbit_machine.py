import requests
import time
from autotrading.machine.base_machine import Machine
import configparser

class KorbitMachine(Machine):

    """
    Class to trade in the Korbit Exchange
    BASE_API_URL: Basic REST API Request URL
    TRADE_CURRENCY_TYPE: Tradable Currency in Korbit
    """
    BASE_API_URL = "https://api.korbit.co.kr"
    TRADE_CURRENCY_TYPE = ["btc", "xrp", "bch", "eth", "ltc", "qtum", "btg", "bsv", "trx", "bat", "eos", "zil", "med",
                           "fet", "etc", "omg", "aergo", "xlm", "dai", "zec", "snt", "bnb", "poly", "usdc", "zrx",
                           "loom", "knc", "mkr", "link"]

    def __init__(self):
        """
        Initiative Method to call the KorbitMachine Class
        Retrieve data from conf/config.ini
        """
        configFileName = '/Users/jaehyunkim/Documents/Workspace/alphanonce_eventdriven_autotrading/conf/config.ini'
        config = configparser.ConfigParser()
        config.read(configFileName)

        self.CLIENT_ID = config['KORBIT']['client_id']
        self.CLIENT_SECRET = config['KORBIT']['client_secret']
        self.USER_NAME = config['KORBIT']['username']
        self.PASSWORD = config['KORBIT']['password']
        self.access_token = None
        self.refresh_token = None
        self.token_type = None
        self.expire = None


    def get_username(self):
        return self.USER_NAME


    def get_nonce(self):
        """
        Method to retrieve the nonce value when retrieving from Private API
        :return: Nonce Value
        """
        return str(int(time.time()))

    def get_token(self):
        """
        Method to retrieve Access Token Info
        :return: Returns Access Tokens if available, Raises Exception
        """
        if self.access_token is not None:
            return self.access_token
        else:
            raise Exception("Need to Set Token")

    def set_token(self, grant_type="password"):
        """
        Method to create AccessToken
        :param grant_type: Password or Refresh Token
        :return: grant_type or raise exception
        """
        token_api_path = "/v1/oauth2/access_token"
        url_path = self.BASE_API_URL + token_api_path
        if grant_type == "password":
            data = {
                "client_id": self.CLIENT_ID,
                "client_secret": self.CLIENT_SECRET,
                "username": self.USER_NAME,
                "password": self.PASSWORD,
                "grant_type": grant_type
            }
        elif grant_type == "refresh_token":
            data = {
                "client_id": self.CLIENT_ID,
                "client_secret": self.CLIENT_SECRET,
                "refresh_token": self.refresh_token,
                "grant_type": grant_type
            }
        else:
            raise Exception("Unexpected grant_type")

        res = requests.post(url_path, data = data)
        result = res.json()
        self.access_token = result["access_token"]
        self.token_type = result["token_type"]
        self.refresh_token = result["refresh_token"]
        self.expire = result["expires_in"]

        return self.expire, self.access_token, self.refresh_token

    def get_ticker(self, currency_type = None):
        """
        Retrieve last executed tick data from orderbook of token
        :param currency_type:  Retrieve type of currency as param TRADE_CURRENCY_TYPE
        :return: Results returned as Dictionary, fields : timestamp, last, bid, ask, high, low,
                 volume, raised exception if nothing is to be returned
        """
        if currency_type is None:
            raise Exception('Need to Input Currency Type')
        time.sleep(1)

        params = {'currency_pair': currency_type}
        ticker_api_path = "/v1/ticker/detailed"

        url_path = self.BASE_API_URL + ticker_api_path
        res = requests.get(url_path, params = params)
        response_json = res.json()
        result = {}
        result["timestamp"] = str(response_json["timestamp"])
        result["last"] = response_json["last"]
        result["bid"] = response_json["bid"]
        result["ask"] = response_json["ask"]
        result["high"] = response_json["high"]
        result["low"] = response_json["low"]
        result["volume"] = response_json["volume"]

        return result


    def get_filled_orders(self, currency_type = None, per = "minute"):

        """
        :param currency_type: String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :param per: String Type, Selection of Time Period to trieve All Execution Infos via minute, hour, or day manner
        :return: All executed information returned in dictionary list type
        """

        if currency_type is None:
            raise Exception("Please Enter Currency Info")

        time.sleep(1)
        params = {'currency_pair': currency_type, 'time': per}
        orders_api_path = "/v1/transactions"
        url_path = self.BASE_API_URL + orders_api_path
        res = requests.get(url_path, params = params)
        result = res.json()

        return result


    def get_constants(self):
        time.sleep(1)
        constants_api_path = "/v1/constants"
        url_path = self.BASE_API_URL + constants_api_path
        res = requests.get(url_path)
        result = res.json()
        self.constants = result

        return result



    def get_wallet_status(self):
        """
        Method to retrieve Wallet Data
        :return:Returns User's wallet balance via dictionary type
        """

        time.sleep(1)
        wallet_status_api_path = "/v1/user/balances"
        url_path = self.BASE_API_URL + wallet_status_api_path
        headers = {"Authorization": "Bearer " + self.access_token}
        res = requests.get(url_path, headers = headers)
        result = res.json()
        wallet_status  = {currency: dict(avail=result[currency]["available"]) for currency in self.TRADE_CURRENCY_TYPE}
        for item in self.TRADE_CURRENCY_TYPE:
            wallet_status[item]["balance"] = str(float(result[item]["trade_in_use"]) + float(result[item]["withdrawal_in_use"]))

        return wallet_status


    def get_list_my_orders(self, currency_type = None):
        """
        Method to retrieve Orders that the user sent
        :param currency_type: String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :return: Returns the user's balance by Currency Type
        """
        if currency_type is None:
            raise Exception("Please input Currency Type")
        time.sleep(1)
        params = {'currency_pair': currency_type}
        list_order_api_path = "/v1/user/order/open"
        url_path = self.BASE_API_URL + list_order_api_path
        headers = {"Authorization": "Bearer " + self.access_token}
        res = requests.get(url_path, headers = headers, params = params)
        result = res.json()

        return result


    def get_my_order_status(self, currency_type = None, order_id = None):
        """
        Method to check the status of the orders that the user sent
        :param currency_type: String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :param order_id: Transaction ID
        :return: Return specific info for order_id
        """
        if currency_type is None or order_id is None:
            raise Exception("Please enter Currency Pair or Order ID")
        time.sleep(1)
        list_transaction_api_path = '/v1/user/orders'
        url_path = self.BASE_API_URL + list_transaction_api_path
        headers = {"Authorization" : "Bearer " + self.access_token}
        params = {"currency_pair": currency_type, "id": order_id}
        res = requests.get(url_path, headers = headers, params = params)
        result = res.json()

        return result




    def buy_order(self, currency_type = None, price = None, qty = None, order_type = "limit"):

        """
        Method to Execute Limit Buy Order. Please note that minimum order amount differs by currency type
        :param currency_type: String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :param price: String Type, 1 Order Amount Equivalent KRW Value
        :param qty: Order Amount
        :param order_type: All order types are limit orders. No Taker Orders
        :return: Returns the status of Orders
        """
        time.sleep(1)
        if currency_type is None or price is None or qty is None:
            raise Exception("Please Check your Params")
        if order_type != "limit":
            raise Exception("Check order type")
        buy_order_api_path = "/v1/user/orders/buy"
        url_path = self.BASE_API_URL + buy_order_api_path
        headers = {"Authorization": "Bearer " + self.access_token}
        data = {"currency_pair": currency_type,
                "type": order_type,
                "price": price,
                "coin_amount": qty,
                "nonce": self.get_nonce()
                }
        res = requests.post(url_path, headers = headers, data = data)
        result = res.json()
        return result




    def sell_order(self, currency_type = None, price = None, qty = None, order_type = "limit"):
        """
        Method to Execute Limit Sell Order. Please note that minimum order amount differs by currency type
        :param currency_type:  String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :param price: String Type, 1 Order Amount Equivalent KRW Value
        :param qty: Order Amount
        :param order_type: All order types are limit orders. No Taker Orders
        :return: Returns the status of Orders
        """
        time.sleep(1)
        if price is None or qty is None or currency_type is None:
            raise Exception("Please Check your Params")
        if order_type != "limit":
            raise Exception("Check order type")

        sell_order_api_path = "/v1/user/orders/sell"
        url_path = self.BASE_API_URL + sell_order_api_path
        headers = {"Authorization":"Bearer " + self.access_token}
        data = {"currency_pair": currency_type,
                "type": order_type,
                "price": price,
                "coin_amount": qty,
                "nonce": self.get_nonce()
                }
        res = requests.post(url_path, headers=headers, data=data)
        result = res.json()
        return result


    def cancel_order(self, currency_type = None, order_id = None):
        """
        Method to Execute Cancel Orders
        :param currency_type: String Type, Inputs for Currency Types which are defined in TRADE_CURRENCY_TYPE
        :param order_id: String Type, Order Info that we are trying to cancel
        :return: Status of Order
        """

        time.sleep(1)
        if currency_type is None or order_id is None:
            raise Exception("Please check your Params")


        cancel_order_api_path = "/v1/user/orders/cancel"
        url_path = self.BASE_API_URL + cancel_order_api_path
        headers = {"Authorization": "Bearer " + self.access_token}
        data = {"currency_pair": currency_type,
                "id": order_id,
                "nonce": self.get_nonce()
                }
        res = requests.post(url_path, headers = headers, data = data)
        result = res.json()
        return result



    def __repr__(self):
        return "(Korbit %s)"%self.USER_NAME

    def __str__(self):
        return str("Korbit")


























