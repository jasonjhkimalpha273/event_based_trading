from abc import ABC, abstractmethod

class Machine(ABC):

    @abstractmethod
    def get_filled_orders(self):
        """
        Method for Filled Order Information
        """
        pass

    @abstractmethod
    def get_ticker(self):
        """
        Method for retrieving the last extracted tick information
        """
        pass

    @abstractmethod
    def get_wallet_status(self):
        """
        Method for querying Wallet Status of user
        """
        pass

    @abstractmethod
    def get_token(self):
        """
        Method for querying access token info
        """
        pass

    @abstractmethod
    def set_token(self):
        """
        Method to create information for Access Tokens
        *Access Tokens = Replacement of ID and PW info when curled via API
        """
        pass

    @abstractmethod
    def get_username(self):
        """
        Method to retrieve user name
        """
        pass

    @abstractmethod
    def buy_order(self):
        """
        Method to execute limit buy order
        """
        pass

    @abstractmethod
    def sell_order(self):
        """
        Method to execute limit sell order
        """
        pass

    @abstractmethod
    def cancel_order(self):
        """
        Method to execute cancel order
        """
        pass

    @abstractmethod
    def get_my_order_status(self):
        """사용자의 주문정보 상세정보를 조회하는 메소드입니다.

        Method to retrieve User Order Information
        """
        pass



